# Lupo

[![Build status](https://ci.appveyor.com/api/projects/status/njaaf85csg9vdadq/branch/master?svg=true)](https://ci.appveyor.com/project/IlmanNafian/lupo/branch/master)

A personalized accounting app.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes.

### Use ready to install binary

The latest version can be downloaded here:
- [Windows](https://ci.appveyor.com/api/projects/IlmanNafian/lupo/artifacts/release.zip?branch=master)

### Building the app by yourself

Building the app require [Node.js](https://nodejs.org/en/download/).

Clone the repository.
```shell script
$ git clone https://gitlab.com/ilmannafian04/lupo.git
$ cd ./lupo
```

Install project's dependency.
```
$ npm install
```

Build the app.
```shell script
$ npm run build
```

Create the binary.
```shell script
$ npm run pack
```

The binary result will be inside `./dist`

## Built With

* [Electron](https://www.electronjs.org/) - Cross-platform JavaScript desktop application framework
* [React](https://reactjs.org/) - Front-end JavScript framework

## Contributing

Just submit an MR.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/ilmannafian04/lupo/tags). 

## Authors

* **Muhamad Ilman Nafian** - [ilmannafian04](https://gitlab.com/ilmannafian04/)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
