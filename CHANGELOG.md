# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.11.0] - 2020-05-30

### Added

- Item stock tracking
- Application logging

### Changed

- Databse location is now at user's document folder
- Transaction now consist of (multiple) item(s)

### Background

There is a massive change in how the app works, first because the model change
and because of massive refacator, both in backend and frontend. Initially, all communication
between react and electron are done using ipc listener. I then decided to use 
handler instead which causes the need to rework most of the communication logic.
In the react side, I decided to refactor everything into function component instead
of class component. I also moved all filtering and pagination job into the backend
query, before this, the app will retrieve ALL data and keep in memory, filtered and
paginated at runtime in memory, this is not scalable at all. On the surface, this
changes doesn't looks like much but this update should greatly improve performance
especially on later time when the database size grows.

## [0.10.0] - 2020-05-21

### Added

- Logger

## [0.9.4] - 2020-05-20

### Fixed

- Fixed `Connection "default" wa not found` error, for real real real now

## [0.9.3] - 2020-05-20

### Fixed

- Fixed `Connection "default" wa not found` error, for real real now

## [0.9.2] - 2020-05-20

### Fixed

- Fixed `Connection "default" wa not found` error, for real now

## [0.9.1] - 2020-05-19

### Fixed

- Fixed `Connection "default" wa not found` error

## [0.9.0] - 2020-05-19

### Changed

- Change how backend works
- Move databse location to user's document folder
- Transaction filtering should now run faster

## [0.8.0] - 2020-03-13

### Added

- Add an ability to export transaction database to a spreadsheet

### Changed

- Change Indonesian translation for `Ledger` from `Buku Besar` to `Buku Kas`
- Change landing page (`/`) to ledger page

## [0.7.1] - 2020-02-15

### Fixed

- Broken translation


## [0.7.0] - 2020-02-15

### Changed

- Move category form, initial balance form, and category table from page Ledger to its own standalone page called Manage

### Fixed

- Fixed wrong year book title caused by wrong translation key


## [0.6.0] - 2020-02-14

### Added

- Internationalization support
  - Currently supported language:
    - English
    - Indonesian


## [0.5.0] - 2020-02-04

### Fixed

- Fix pressing enter when filling category form will reload the page.

### Added

- Pressing enter in category form and transaction form will submit the form if the form is valid.


## [0.4.0] - 2020-01-11

### Added

- Year book page. Display the yearly report for each categories.

### Changed

- Extract common string formatting method to a utility file in util folder.

### Fixed

- Fix transaction form submiting date in format "YYYY-MM-DD HH-mm-ss" instead of "YYYY-MM-DD HH:mm:ss".

[Unreleased]: https://gitlab.com/ilmannafian04/lupo/compare/master...staging
[0.11.0]: https://gitlab.com/ilmannafian04/lupo/compare/20adfbba...57618f87
[0.10.0]: https://gitlab.com/ilmannafian04/lupo/compare/d8e4a8b8...20adfbba
[0.9.4]: https://gitlab.com/ilmannafian04/lupo/compare/0791e633...d8e4a8b8
[0.9.3]: https://gitlab.com/ilmannafian04/lupo/compare/fac4eb11...0791e633
[0.9.2]: https://gitlab.com/ilmannafian04/lupo/compare/b172696b...fac4eb11
[0.9.1]: https://gitlab.com/ilmannafian04/lupo/compare/462aea17...b172696b
[0.9.0]: https://gitlab.com/ilmannafian04/lupo/compare/e7b31e63...462aea17
[0.8.0]: https://gitlab.com/ilmannafian04/lupo/compare/87ce2e96...e7b31e63
[0.7.1]: https://gitlab.com/ilmannafian04/lupo/compare/50130f19...87ce2e96
[0.7.0]: https://gitlab.com/ilmannafian04/lupo/compare/38f9c829...50130f19
[0.6.0]: https://gitlab.com/ilmannafian04/lupo/compare/64e538e5...38f9c829
[0.5.0]: https://gitlab.com/ilmannafian04/lupo/compare/89bc8e5d...64e538e5
[0.4.0]: https://gitlab.com/ilmannafian04/lupo/compare/51300cf4...d56e2f2b
