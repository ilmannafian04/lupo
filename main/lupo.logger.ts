import ElectronLog from 'electron-log';
import { Logger } from 'typeorm';

export default class LupoLogger implements Logger {
    log(level: 'log' | 'info' | 'warn', message: any): any {
        switch (level) {
            case 'info': {
                ElectronLog.log(message);
                break;
            }
            case 'log': {
                ElectronLog.log(message);
                break;
            }
            case 'warn': {
                ElectronLog.warn(message);
                break;
            }
            default: {
                ElectronLog.log(message);
                break;
            }
        }
    }

    logMigration(message: string): any {
        ElectronLog.verbose(message);
    }

    logQuery(query: string, parameters?: any[]): any {
        if (parameters) ElectronLog.verbose(query, parameters);
        else ElectronLog.verbose(query);
    }

    logQueryError(error: string, query: string, parameters?: any[]): any {
        if (parameters) ElectronLog.error(error, query, parameters);
        else ElectronLog.error(error, query);
    }

    logQuerySlow(time: number, query: string, parameters?: any[]): any {
        if (parameters) ElectronLog.verbose(time, query, parameters);
        else ElectronLog.verbose(time, query);
    }

    logSchemaBuild(message: string): any {
        ElectronLog.verbose(message);
    }
}
