import { app } from 'electron';
import electronIsDev from 'electron-is-dev';
import { renameSync } from 'fs';
import moment from 'moment';
import path from 'path';

export const logRotator = (file): void => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    // declaration file is wrong, this is not a string
    const oldPath = file.path ? file.path : file;
    const info = path.parse(oldPath);
    try {
        renameSync(oldPath, path.join(info.dir, info.name + moment().format('-YYYY-MM-DDTHH-mm') + info.ext));
    } catch (e) {
        console.log('Could not rotate log', e);
    }
};

export const getLogPath = (process): string =>
    electronIsDev
        ? path.join(__dirname, '..', '..', 'log', `${process}.log`)
        : path.join(app.getPath('documents'), 'Lupo', 'log', `${process}.log`);
