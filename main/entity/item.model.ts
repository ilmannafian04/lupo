import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import Category from './category.model';

@Entity()
export default class Item {
    @PrimaryGeneratedColumn() id: number;
    @Column({ unique: true }) name: string;
    @Column() price: number;
    @Column() restockPrice: number;
    @ManyToOne(() => Category, { nullable: false, eager: true, onDelete: 'CASCADE' }) category: Category;
    @Column({ default: 0 }) stock: number;
}
