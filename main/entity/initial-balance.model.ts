import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export default class InitialBalance {
    @PrimaryGeneratedColumn() id: number;
    @Column({ default: 0 }) balance: number;
}
