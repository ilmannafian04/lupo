import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import TransactionItem from './transaction-item.model';

@Entity()
export default class Transaction {
    @PrimaryGeneratedColumn() id: number;
    @Column('text', { nullable: true }) notes?: string;
    @Column() date: Date;
    @CreateDateColumn() createdDate: Date;
    @OneToMany(() => TransactionItem, (transactionItem) => transactionItem.transaction, {
        nullable: false,
        cascade: true,
        eager: true,
        onDelete: 'CASCADE',
    })
    items: TransactionItem[];
    @Column({ default: 0 }) total: number;
    @Column({ default: false }) restock: boolean;
}
