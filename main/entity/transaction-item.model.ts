import { Column, Entity, ManyToOne } from 'typeorm';

import Item from './item.model';
import Transaction from './transaction.model';

@Entity()
export default class TransactionItem {
    @ManyToOne(() => Item, { nullable: false, onDelete: 'CASCADE', eager: true, cascade: ['update'], primary: true })
    item: Item;
    @ManyToOne(() => Transaction, {
        onDelete: 'CASCADE',
        nullable: false,
        deferrable: 'INITIALLY DEFERRED',
        primary: true,
    })
    transaction: Transaction;
    @Column() count: number;
    @Column({ default: 0 }) total: number;
}
