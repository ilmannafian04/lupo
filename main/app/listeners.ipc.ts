import { dialog, ipcMain } from 'electron';
import ElectronLog from 'electron-log';
import moment from 'moment';
import { FindManyOptions, getRepository } from 'typeorm';
import xlsx from 'xlsx';

import Category from '../entity/category.model';
import InitialBalance from '../entity/initial-balance.model';
import Transaction from '../entity/transaction.model';
import Item from '../entity/item.model';
import TransactionItem from '../entity/transaction-item.model';
import { getLogPath, logRotator } from '../util/log.util';

ipcMain.on('react', () => {
    ElectronLog.log('React loaded');
});

ipcMain.on('test', (event) => {
    event.reply('test-ret');
});

ipcMain.handle('submit-transaction', (event, transactionDto) => {
    const repository = getRepository(Transaction);
    const transactionEntity = repository.merge(new Transaction(), transactionDto);
    return repository
        .save(transactionEntity)
        .then((result) => {
            return repository.findOne({ id: result.id });
        })
        .then((transaction) => {
            transaction.items.forEach((transactionItem) => {
                const totalPerItem =
                    transactionItem.count *
                    (transaction.restock ? transactionItem.item.restockPrice : transactionItem.item.price);
                transaction.total += totalPerItem;
                transactionItem.total = totalPerItem;
                transactionItem.item.stock += transaction.restock ? transactionItem.count : -transactionItem.count;
            });
            return repository.save(transaction);
        });
});

ipcMain.on('get-transactions', (event, args) => {
    const filter: FindManyOptions = {};
    if (args) {
        if (args.limit !== undefined) filter.take = args.limit;
        if (args.year !== undefined) filter.where = `strftime('%Y', "transaction".date) = '${args.year}'`;
    }
    getRepository(Transaction)
        .find(filter)
        .then((transactions) => {
            event.reply('transactions', transactions);
        })
        .catch((error) => {
            ElectronLog.error(error);
            event.reply('transactions', { error: error });
        });
});

ipcMain.handle('transaction/get', (event, filter) => {
    let query = getRepository(Transaction).createQueryBuilder('t');
    query.leftJoinAndSelect('t.items', 'ti').leftJoinAndSelect('ti.item', 'i').orderBy('t.date', 'DESC');
    if (filter) {
        query.where(filter.id ? 't.id = :tId' : '1 = 1', { tId: filter.id });
        if (filter.items.length > 0) query.andWhere('i.id in (:...items)', { items: filter.items });
        if (filter.total.from !== '') query.andWhere('t.total >= :totalFrom', { totalFrom: filter.total.from });
        if (filter.total.until !== '') query.andWhere('t.total <= :totalUntil', { totalUntil: filter.total.until });
        if (filter.restock !== '') query.andWhere('t.restock = :restock', { restock: filter.restock });
        if (filter.date.from !== '') {
            query.andWhere('t.date >= :dateFrom', {
                dateFrom: `${moment(filter.date.from).format('YYYY-MM-DD')}`,
            });
        }
        if (filter.date.until !== '') {
            query.andWhere('t.date <= :dateUntil', {
                dateUntil: `${moment(filter.date.until).format('YYYY-MM-DD')} 1`,
            });
        }
        if (filter.notes) {
            if (filter.notes == '$$') query = query.andWhere('t.notes is null');
            else query.andWhere(`t.notes like :note`, { note: `%${filter.notes}%` });
        }
        if (filter.transactionPerPage) {
            query.take(filter.transactionPerPage);
            if (filter.page) query.skip(filter.transactionPerPage * (filter.page - 1));
        }
    }
    return filter && filter.count ? query.getManyAndCount() : query.getMany();
});

ipcMain.handle('update-transaction', (event, transactionDto) => {
    const repository = getRepository(Transaction);
    const transactionItemRepository = getRepository(TransactionItem);
    return repository
        .findOne({ id: transactionDto.id })
        .then((transaction) => {
            transaction.items.forEach((transactionItem) => {
                transactionItem.item.stock += transaction.restock ? -transactionItem.count : transactionItem.count;
            });
            return repository.save(transaction);
        })
        .then((transaction) => transactionItemRepository.delete({ transaction }))
        .then(() => repository.findOne({ id: transactionDto.id }))
        .then((transaction) => {
            transaction.items = [];
            repository.merge(transaction, transactionDto);
            return repository.save(transaction);
        })
        .then(() => repository.findOne({ id: transactionDto.id }))
        .then((transaction) => {
            transaction.total = 0;
            transaction.items.forEach((transactionItem) => {
                if (transaction.restock) {
                    const temp = transactionItem.count * transactionItem.item.restockPrice;
                    transaction.total += temp;
                    transactionItem.total = temp;
                } else {
                    const temp = transactionItem.count * transactionItem.item.price;
                    transaction.total += temp;
                    transactionItem.total = temp;
                }
                transactionItem.item.stock += transaction.restock ? transactionItem.count : -transactionItem.count;
            });
            return repository.save(transaction);
        });
});

ipcMain.handle('delete-transaction', (event, id) => getRepository(Transaction).delete({ id }));

ipcMain.handle('yearBook/yearList', () =>
    getRepository(Transaction)
        .createQueryBuilder('t')
        .select(`distinct strftime('%Y', t.date)`, 'year')
        .orderBy('year', 'DESC')
        .getRawMany(),
);

ipcMain.handle('yearBook/getData', (event, year) => {
    return getRepository(TransactionItem)
        .createQueryBuilder('ti')
        .select('sum(ti.total)', 'total')
        .addSelect('t.restock', 'restock')
        .addSelect('i.name', 'name')
        .addSelect(`strftime('%m', t.date)`, 'month')
        .leftJoin('ti.transaction', 't')
        .leftJoin('ti.item', 'i')
        .where(`strftime('%Y', t.date) = :year`, { year })
        .groupBy('i.name')
        .addGroupBy(`strftime('%m', t.date)`)
        .addGroupBy('t.restock')
        .getRawMany();
});

ipcMain.handle('submit-category', (event, args) => {
    const repository = getRepository(Category);
    return repository.save(repository.merge(new Category(), args));
});

ipcMain.on('get-categories', (event) => {
    const repository = getRepository(Category);
    repository
        .find()
        .then((rows) => {
            event.reply('categories', rows);
        })
        .catch((error) => {
            ElectronLog.error(error);
            event.reply('categories', { error: error });
        });
});

ipcMain.handle('category/get', (event, filter?) => {
    const query = getRepository(Category).createQueryBuilder('c').orderBy('c.name', 'ASC');
    if (filter && filter.categoryPerPage) {
        query.take(filter.categoryPerPage);
        if (filter.page) query.skip(filter.categoryPerPage * (filter.page - 1));
    }
    return filter && filter.count ? query.getManyAndCount() : query.getMany();
});

ipcMain.handle('update-category', (event, args) => {
    const repository = getRepository(Category);
    return repository.findOne({ id: args.id }).then((category) => repository.save(repository.merge(category, args)));
});

ipcMain.handle('delete-category', (event, args) => {
    const repository = getRepository(Category);
    repository.findOne({ id: args }).then((category) => repository.remove(category));
});

ipcMain.handle('balance/get', () => getRepository(InitialBalance).find());

ipcMain.handle('balance/set', (event, balance) => {
    const repository = getRepository(InitialBalance);
    return repository.find().then((balances) => {
        balances[0].balance = balance;
        return repository.save(balances[0]);
    });
});

ipcMain.handle('export-database', () => {
    const repository = getRepository(Transaction);
    return dialog
        .showSaveDialog({
            title: 'Export database to',
            filters: [
                {
                    name: 'Spreadsheets',
                    extensions: 'xls|xlsx|xlsm|xlsb|xml|csv|txt|dif|sylk|slk|prn|ods|fods|htm|html'.split('|'),
                },
            ],
        })
        .then((saveDialogReturnValue) => {
            return repository.find().then((transactions) => {
                const workbook = xlsx.utils.book_new();
                const worksheet = xlsx.utils.json_to_sheet(
                    transactions.map((transaction) => {
                        return {
                            Notes: transaction.notes ? transaction.notes : '',
                            Date: transaction.date,
                            Items: transaction.items.reduce(
                                (result, tItem, index) =>
                                    `${result}${tItem.count} x ${tItem.item.name} Rp${tItem.total}${
                                        index === transaction.items.length - 1 ? '' : '\n'
                                    }`,
                                '',
                            ),
                            Total: `Rp${transaction.total}`,
                            Restock: transaction.restock,
                        };
                    }),
                );
                xlsx.utils.book_append_sheet(workbook, worksheet, 'Transactions');
                xlsx.writeFile(workbook, saveDialogReturnValue.filePath);
            });
        });
});

ipcMain.on('get-items', (event) => {
    getRepository(Item)
        .find()
        .then((items) => {
            event.reply('items', items);
        })
        .catch((error) => {
            ElectronLog.error(error);
            event.reply('items', { error });
        });
});

ipcMain.handle('item/get', (event, filter?) => {
    const query = getRepository(Item).createQueryBuilder('i');
    query.leftJoinAndSelect('i.category', 'c').orderBy('i.name', 'ASC');
    if (filter) {
        query.where(filter.id ? 'i.id = :iId' : '1 = 1', { iId: filter.id });
        if (filter.name !== '') query.andWhere('i.name like :iName', { iName: `%${filter.name}%` });
        if (filter.categories.length > 0) query.andWhere('c.id in (:...cId)', { cId: filter.categories });
        if (filter.stock.from !== '') query.andWhere('i.stock >= :stockFrom', { stockFrom: filter.stock.from });
        if (filter.stock.until !== '') query.andWhere('i.stock <= :stockUntil', { stockUntil: filter.stock.until });
        if (filter.price.from !== '') query.andWhere('i.price >= :priceFrom', { priceFrom: filter.price.from });
        if (filter.price.until !== '') query.andWhere('i.price <= :priceUntil', { priceUntil: filter.price.until });
        if (filter.restockPrice.from !== '') {
            query.andWhere('i.restockPrice >= :restockPriceFrom', { restockPriceFrom: filter.restockPrice.from });
        }
        if (filter.restockPrice.until !== '') {
            query.andWhere('i.restockPrice <= :restockPriceUntil', { restockPriceUntil: filter.restockPrice.until });
        }
        if (filter.itemPerPage) {
            query.take(filter.itemPerPage);
            if (filter.page) query.skip(filter.itemPerPage * (filter.page - 1));
        }
    }
    return filter && filter.count ? query.getManyAndCount() : query.getMany();
});

ipcMain.handle('submit-item', (event, item) => {
    const repository = getRepository(Item);
    return repository.save(repository.merge(new Item(), item));
});

ipcMain.handle('update-item', (event, args) => {
    const repository = getRepository(Item);
    return repository.findOne({ id: args.id }).then((item) => repository.save(repository.merge(item, args)));
});

ipcMain.handle('delete-item', (event, id: number) => {
    const repository = getRepository(Item);
    return repository.findOne({ id }).then((item) => repository.remove(item));
});

ipcMain.handle('ledger/todaysSummary', () => {
    const promises = [];
    const repository = getRepository(Transaction);
    promises.push(
        repository
            .createQueryBuilder('t')
            .select('sum(t.total)', 'todaysSale')
            .where(`strftime('%Y-%m-%d', t.date) = :today`, { today: moment.utc().format('YYYY-MM-DD') })
            .andWhere('t.restock = false')
            .getRawOne(),
    );
    promises.push(
        repository
            .createQueryBuilder('t')
            .select('sum(t.total)', 'todaysRestock')
            .where(`strftime('%Y-%m-%d', t.date) = :today`, { today: moment.utc().format('YYYY-MM-DD') })
            .andWhere('t.restock = true')
            .getRawOne(),
    );
    promises.push(
        repository.createQueryBuilder('t').select('sum(t.total)', 'sale').andWhere('t.restock = false').getRawOne(),
    );
    promises.push(
        repository.createQueryBuilder('t').select('sum(t.total)', 'restock').andWhere('t.restock = true').getRawOne(),
    );
    promises.push(getRepository(InitialBalance).find());
    return Promise.all(promises);
});

ipcMain.handle('log/getPath', () => {
    return getLogPath('renderer');
});

ipcMain.handle('log/rotate', (event, file) => {
    logRotator(file);
});
