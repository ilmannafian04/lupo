import { ipcRenderer, IpcRenderer } from 'electron';
import ElectronLog, { ElectronLog as ElectronLogInterface } from 'electron-log';

declare global {
    interface Window {
        ipcRenderer: IpcRenderer;
        elog: ElectronLogInterface;
    }
}

window.ipcRenderer = ipcRenderer;
window.elog = ElectronLog;
