import { app, BrowserWindow, globalShortcut } from 'electron';
import electronIsDev from 'electron-is-dev';
import ElectronLog from 'electron-log';
import path from 'path';
import { createConnection, getRepository } from 'typeorm';

import './app/listeners.ipc';
import Category from './entity/category.model';
import InitialBalance from './entity/initial-balance.model';
import Item from './entity/item.model';
import Transaction from './entity/transaction.model';
import TransactionItem from './entity/transaction-item.model';
import LupoLogger from './lupo.logger';
import { getLogPath, logRotator } from './util/log.util';

export default class Lupo {
    static win: BrowserWindow;
    static devToolOpened = false;

    static main(): void {
        if (app.isReady()) {
            Lupo.createWindow();
        } else {
            app.on('ready', Lupo.createWindow);
        }
        app.whenReady()
            .then(() => {
                ElectronLog.transports.file.resolvePath = (): string => getLogPath('main');
                ElectronLog.transports.file.archiveLog = logRotator;
                ElectronLog.info('App ready');
                electronIsDev
                    ? ElectronLog.log('Development environtment detect')
                    : ElectronLog.log('Production environtment detect');
                return createConnection({
                    name: 'default',
                    type: 'sqlite',
                    synchronize: true,
                    database: electronIsDev
                        ? path.join(__dirname, '..', 'main.sqlite')
                        : path.join(app.getPath('documents'), 'Lupo', 'main.sqlite'),
                    entities: [Category, InitialBalance, Item, Transaction, TransactionItem],
                    logging: electronIsDev ? true : ['error', 'schema', 'warn', 'info', 'log'],
                    logger: new LupoLogger(),
                });
            })
            .then(() => getRepository(InitialBalance).save(new InitialBalance()))
            .then(() => {
                ElectronLog.log('DB initiated');
                globalShortcut.register('CommandOrControl+Shift+I', () => {
                    if (Lupo.devToolOpened) {
                        Lupo.win.webContents.closeDevTools();
                        Lupo.devToolOpened = false;
                    } else {
                        Lupo.win.webContents.openDevTools();
                        Lupo.devToolOpened = true;
                    }
                });
            })
            .catch((error) => {
                ElectronLog.error(error);
                app.exit(1);
            });
    }

    static createWindow(): void {
        Lupo.win = new BrowserWindow({
            width: 1280,
            height: 720,
            webPreferences: {
                nodeIntegration: true,
                preload: path.join(__dirname, 'app', 'preload.js'),
            },
        });
        Lupo.win.maximize();
        Lupo.win.removeMenu();
        Lupo.win.loadURL(electronIsDev ? 'http://localhost:3000' : `file://${path.join(__dirname, './index.html')}`);
        Lupo.win.on('closed', (): void => (Lupo.win = null));
        if (electronIsDev) {
            Lupo.win.webContents.on('dom-ready', () => {
                Lupo.win.webContents.openDevTools();
                Lupo.devToolOpened = true;
            });
            Lupo.win.webContents.on('devtools-reload-page', () => {
                Lupo.win.webContents.closeDevTools();
                Lupo.devToolOpened = false;
            });
        }
    }
}
