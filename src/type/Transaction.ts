import Item from './Item';

export interface TransactionItem {
    id: number;
    item: Item;
    count: number;
}

export default interface Transaction {
    id: number;
    notes?: string;
    date: string;
    createdDate: string;
    items: TransactionItem[];
    total?: number;
    restock: boolean;
}
