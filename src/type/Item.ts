import Category from './Category';

export default interface Item {
    id: number;
    name: string;
    price: number;
    restockPrice: number;
    category: Category;
    stock?: number;
}
