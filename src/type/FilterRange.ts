export default interface FilterRange<T> {
    from: T;
    until: T;
}
