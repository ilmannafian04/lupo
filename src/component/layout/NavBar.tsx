import React, { FunctionComponent, useState } from 'react';
import i18next from 'i18next';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useTranslation } from 'react-i18next';
import { LinkContainer } from 'react-router-bootstrap';

const changeLanguange = (ln: string): void => {
    i18next.changeLanguage(ln).then(() => null);
};

const NavBar: FunctionComponent = () => {
    const { t } = useTranslation();
    const [ln, setLn] = useState('id');
    const changeLanguageHandler = (key: string): void => {
        changeLanguange(key);
        setLn(key);
    };
    return (
        <Navbar variant="dark" bg="dark" sticky="top">
            <LinkContainer to="/">
                <Navbar.Brand>Lupo</Navbar.Brand>
            </LinkContainer>
            <Nav className="mr-auto">
                <LinkContainer to="/">
                    <Nav.Link>{t('basic.ledger')}</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/items">
                    <Nav.Link>{t('basic.stock')}</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/yearbook">
                    <Nav.Link>{t('basic.yearBook')}</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/manage">
                    <Nav.Link>{t('basic.manage')}</Nav.Link>
                </LinkContainer>
            </Nav>
            <DropdownButton id="dropdown-basic-button" title={t('basic.language')} variant="outline-light">
                <Dropdown.Item eventKey="en" onSelect={changeLanguageHandler} active={ln === 'en'}>
                    EN
                </Dropdown.Item>
                <Dropdown.Item eventKey="id" onSelect={changeLanguageHandler} active={ln === 'id'}>
                    ID
                </Dropdown.Item>
            </DropdownButton>
        </Navbar>
    );
};

export default NavBar;
