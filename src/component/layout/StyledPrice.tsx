import React, { FunctionComponent } from 'react';

import { formatCurrency } from '../../util/StringFormatting';

interface StyledPriceProps {
    price: number;
    isRestock: boolean;
}

const StyledPrice: FunctionComponent<StyledPriceProps> = (props: StyledPriceProps) => {
    return (
        <>
            <span style={{ color: props.isRestock ? 'red' : 'green' }}>
                {props.isRestock ? '-' : '+'}
                {formatCurrency(props.price)}
            </span>
        </>
    );
};

export default StyledPrice;
