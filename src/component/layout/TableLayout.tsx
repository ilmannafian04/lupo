import React, { FunctionComponent, ReactElement } from 'react';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Pagination from 'react-bootstrap/Pagination';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';

interface ItemTableProps {
    header: string[];
    data: (string | ReactElement)[][];
    editMode: boolean;
    buttons: ActionButton[][];
    currentPage: number;
    pageCount: number;
    changePageFn: (page: number) => void;
}

const TableLayout: FunctionComponent<ItemTableProps> = (props: ItemTableProps) => {
    const pagination = [
        <Pagination.Item
            key={1}
            active={1 === props.currentPage}
            onClick={(): void => {
                props.changePageFn(1);
            }}
        >
            1
        </Pagination.Item>,
    ];
    for (let i = 2; i <= props.pageCount; i++) {
        pagination.push(
            <Pagination.Item
                key={i}
                active={i === props.currentPage}
                onClick={(): void => {
                    props.changePageFn(i);
                }}
            >
                {i}
            </Pagination.Item>,
        );
    }
    return (
        <>
            <Table>
                <thead>
                    <tr>
                        {props.header.map((header, index) => (
                            <th key={index}>{header}</th>
                        ))}
                        {props.editMode ? <th>Edit</th> : null}
                    </tr>
                </thead>
                <tbody>
                    {props.data.map((row, index) => (
                        <tr key={index}>
                            {row.map((cell, index) => (
                                <td key={index}>
                                    {typeof cell === 'string' ? (
                                        /\n/.test(cell) ? (
                                            <ul>
                                                {cell
                                                    .split('\n')
                                                    .map((splitCell, index2) =>
                                                        splitCell ? <li key={index2}>{splitCell}</li> : null,
                                                    )}
                                            </ul>
                                        ) : (
                                            cell
                                        )
                                    ) : (
                                        cell
                                    )}
                                </td>
                            ))}
                            {props.editMode ? (
                                <td>
                                    <ButtonGroup>
                                        {props.buttons[index].map((button, index2) => {
                                            return (
                                                <Button
                                                    key={`${index}-${index2}`}
                                                    onClick={button.fn}
                                                    variant={button.variant}
                                                >
                                                    {button.name}
                                                </Button>
                                            );
                                        })}
                                    </ButtonGroup>
                                </td>
                            ) : null}
                        </tr>
                    ))}
                </tbody>
            </Table>
            {props.pageCount > 1 ? (
                <Row className="justify-content-center">
                    <Pagination>{pagination}</Pagination>
                </Row>
            ) : null}
        </>
    );
};

export interface ActionButton {
    name: string;
    variant:
        | 'primary'
        | 'secondary'
        | 'success'
        | 'danger'
        | 'warning'
        | 'info'
        | 'dark'
        | 'light'
        | 'link'
        | 'outline-primary'
        | 'outline-secondary'
        | 'outline-success'
        | 'outline-danger'
        | 'outline-warning'
        | 'outline-info'
        | 'outline-dark'
        | 'outline-light';
    fn: () => void;
}

export default TableLayout;
