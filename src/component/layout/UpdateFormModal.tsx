import React, { FunctionComponent, ReactNode } from 'react';
import Modal from 'react-bootstrap/Modal';
import ModalTitle from 'react-bootstrap/ModalTitle';

type UpdateFormModalProps = {
    hideHandler: () => void;
    isVisible: boolean;
    form: ReactNode;
    headerTitle: string;
};

const UpdateFormModal: FunctionComponent<UpdateFormModalProps> = (props: UpdateFormModalProps) => {
    return (
        <Modal size="lg" centered onHide={props.hideHandler} show={props.isVisible}>
            <Modal.Header closeButton>
                <ModalTitle>{props.headerTitle}</ModalTitle>
            </Modal.Header>
            <Modal.Body>{props.form}</Modal.Body>
        </Modal>
    );
};

export default UpdateFormModal;
