import React, { FunctionComponent } from 'react';
import Container from 'react-bootstrap/Container';

const Landing: FunctionComponent = () => {
    return (
        <Container>
            <h1>Hello</h1>
        </Container>
    );
};

export default Landing;
