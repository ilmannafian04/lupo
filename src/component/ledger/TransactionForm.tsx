import produce from 'immer';
import moment from 'moment';
import React, { ChangeEvent, FormEvent, FunctionComponent, useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';

import Item from '../../type/Item';
import Transaction from '../../type/Transaction';
import { formatCurrency } from '../../util/StringFormatting';
import notify from '../../util/Toast';

interface TransactionItemForm {
    id: number;
    count: number;
}

interface TransactionFormDto {
    id?: number;
    notes?: string;
    date: Date;
    createdDate?: Date;
    items: { item: number; count: number }[];
    restock: boolean;
}

interface FormState {
    notes?: string;
    date: string;
    items: TransactionItemForm[];
    addItem: TransactionItemForm;
    restock: boolean;
}

interface TransactionFormProps {
    forceRefreshFn: () => void;
    editTransaction?: Transaction;
}

const addItemInitialState = { id: -1, count: 0 };
const todayDate = (): string => moment(new Date()).format('YYYY-MM-DD');
const formatValidator = (formState: FormState): boolean => {
    if (moment(formState.date).toDate() > new Date()) return false;
    if (formState.items.length === 0) return false;
    return formState.items.reduce((isValid: boolean, item) => isValid && item.count > 0, true);
};
const cleanFormState: FormState = {
    notes: '',
    date: todayDate(),
    items: [],
    addItem: addItemInitialState,
    restock: false,
};

const TransactionForm: FunctionComponent<TransactionFormProps> = (props: TransactionFormProps) => {
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [items, setItems] = useState<Item[]>([]);
    const [formState, setFormState] = useState<FormState>(
        props.editTransaction === undefined
            ? cleanFormState
            : {
                  notes: props.editTransaction.notes ? props.editTransaction.notes : '',
                  date: moment(props.editTransaction.date).format('YYYY-MM-DD'),
                  items: props.editTransaction.items.map((transactionItem) => {
                      return { id: transactionItem.item.id, count: transactionItem.count };
                  }),
                  addItem: cleanFormState.addItem,
                  restock: props.editTransaction.restock,
              },
    );
    const { t } = useTranslation();
    useEffect(() => {
        window.ipcRenderer
            .invoke('item/get')
            .then((result) => setItems(result))
            .catch((error) => console.error(error));
    }, []);
    useEffect(() => {
        if (formState.addItem.id !== -1) {
            setFormState(
                produce(formState, (draft) => {
                    draft.items.push({ id: formState.addItem.id, count: formState.addItem.count || 1 });
                    draft.addItem = addItemInitialState;
                }),
            );
        }
    }, [formState]);
    const availableItem = (currentId: number): Item[] =>
        items.filter(
            (item) =>
                currentId === item.id ||
                formState.items.reduce((isNotUsed: boolean, item2) => isNotUsed && item.id !== item2.id, true),
        );
    const inputChangeHandler = (event: ChangeEvent<HTMLInputElement>): void => {
        const target = event.target;
        setFormState(
            produce(formState, (draft) => {
                if (target.name === 'notes') draft.notes = target.value;
                else if (target.name === 'date') draft.date = target.value;
                else if (target.name === 'addItemId') draft.addItem.id = Number(target.value);
                else if (target.name === 'addItemCount' && Number(target.value) >= 0) {
                    draft.addItem.count = Number(target.value);
                } else if (target.name.split('-')[0] === 'itemId') {
                    draft.items[Number(target.name.split('-')[1])].id = Number(target.value);
                } else if (target.name.split('-')[0] === 'itemCount') {
                    draft.items[Number(target.name.split('-')[1])].count = Number(target.value);
                } else {
                    draft.restock = !formState.restock;
                }
            }),
        );
    };
    const deleteItem = (index: number): void => {
        setFormState(
            produce(formState, (draft) => {
                draft.items.splice(index, 1);
            }),
        );
    };
    const submitHandler = (event: FormEvent<HTMLFormElement>): void => {
        event.preventDefault();
        setIsSubmitting(true);
        const transaction: TransactionFormDto = {
            id: props.editTransaction ? props.editTransaction.id : undefined,
            date: moment.utc(formState.date, 'YYYY-MM-DD').toDate(),
            createdDate: props.editTransaction ? moment(props.editTransaction.createdDate).toDate() : undefined,
            items: formState.items.map((item) => {
                return {
                    item: item.id,
                    count: item.count,
                };
            }),
            restock: formState.restock,
        };
        if (formState.notes.length > 0) transaction.notes = formState.notes;
        window.ipcRenderer
            .invoke(props.editTransaction ? 'update-transaction' : 'submit-transaction', transaction)
            .then(() => {
                if (!props.editTransaction) setFormState(cleanFormState);
                props.forceRefreshFn();
                notify(t('basic.success'));
            })
            .catch((error) => {
                notify(error, 'error');
                console.error(error);
            })
            .then(() => setIsSubmitting(false));
    };
    return (
        <Form onSubmit={submitHandler}>
            <Form.Row>
                <Col>
                    <Form.Row>
                        <Form.Group as={Col} controlId="date">
                            <Form.Label>{t('basic.date')}</Form.Label>
                            <Form.Control
                                type="date"
                                name="date"
                                value={formState.date}
                                onChange={inputChangeHandler}
                                max={todayDate()}
                            />
                        </Form.Group>
                    </Form.Row>
                    <Form.Group controlId="name">
                        <Form.Label>{t('basic.notes')}</Form.Label>
                        <Form.Control
                            as="textarea"
                            name="notes"
                            value={formState.notes}
                            onChange={inputChangeHandler}
                        />
                    </Form.Group>
                </Col>
                <Col>
                    <Form.Label>{t('basic.item')}</Form.Label>
                    <div>
                        {formState.items.map((item, index) => (
                            <Form.Group key={index}>
                                <Form.Row>
                                    <Col md="auto">
                                        <Button onClick={(): void => deleteItem(index)} variant="danger">
                                            -
                                        </Button>
                                    </Col>
                                    <Col>
                                        <Form.Control
                                            as="select"
                                            name={`itemId-${index}`}
                                            value={formState.items[index].id}
                                            onChange={inputChangeHandler}
                                        >
                                            {availableItem(item.id).map((item, index) => (
                                                <option key={index} value={item.id}>
                                                    {item.name}&nbsp;
                                                    {formatCurrency(formState.restock ? item.restockPrice : item.price)}
                                                </option>
                                            ))}
                                        </Form.Control>
                                    </Col>
                                    <Col>
                                        <Form.Control
                                            type="number"
                                            name={`itemCount-${index}`}
                                            value={formState.items[index].count}
                                            onChange={inputChangeHandler}
                                        />
                                    </Col>
                                </Form.Row>
                            </Form.Group>
                        ))}
                        <Form.Group key={formState.items.length + 1}>
                            <Form.Row>
                                <Col md="auto" style={{ visibility: 'hidden' }}>
                                    <Button variant="danger">-</Button>
                                </Col>
                                <Col>
                                    <Form.Control
                                        as="select"
                                        name="addItemId"
                                        value={formState.addItem.id}
                                        onChange={inputChangeHandler}
                                    >
                                        <option value="-1">{t('basic.pick')}</option>
                                        {availableItem(-1).map((item, index) => (
                                            <option key={++index} value={item.id}>
                                                {item.name}&nbsp;
                                                {formatCurrency(formState.restock ? item.restockPrice : item.price)}
                                            </option>
                                        ))}
                                    </Form.Control>
                                </Col>
                                <Col>
                                    <Form.Control
                                        type="number"
                                        name="addItemCount"
                                        value={formState.addItem.count}
                                        onChange={inputChangeHandler}
                                    />
                                </Col>
                            </Form.Row>
                        </Form.Group>
                    </div>
                </Col>
            </Form.Row>
            <Form.Group controlId="restock">
                <Form.Check
                    type="checkbox"
                    name="restock"
                    label={t('basic.restock')}
                    defaultChecked={formState.restock}
                    onChange={inputChangeHandler}
                />
            </Form.Group>
            <Button disabled={!(formatValidator(formState) && !isSubmitting)} type="submit">
                {t('basic.submit')}
            </Button>
        </Form>
    );
};

export default TransactionForm;
