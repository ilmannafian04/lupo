import moment from 'moment';
import React, { ChangeEvent, FunctionComponent, useEffect, useState } from 'react';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { useTranslation } from 'react-i18next';

import { formatThousand } from '../../util/StringFormatting';
import FilterRange from '../../type/FilterRange';
import Item from '../../type/Item';

interface TransactionFilterProps {
    changeHandler: (event: ChangeEvent<HTMLInputElement>) => void;
    filterState: TransactionFilterState;
}

export interface TransactionFilterState {
    id: number | '';
    notes: string;
    items: number[];
    total: FilterRange<number | ''>;
    date: FilterRange<Date | ''>;
    isRestock: boolean;
    isSale: boolean;
}

const TransactionFilter: FunctionComponent<TransactionFilterProps> = (props: TransactionFilterProps) => {
    const [items, setItems] = useState<Item[]>([]);
    const { t } = useTranslation();
    useEffect(() => {
        window.ipcRenderer
            .invoke('item/get')
            .then((result) => setItems(result))
            .catch((error) => console.error(error));
    }, []);
    return (
        <Form.Row>
            <Col>
                <Form.Row>
                    <Form.Group as={Col} controlId="filterId" md={2}>
                        <Form.Label>ID</Form.Label>
                        <Form.Control name="id" value={props.filterState.id} onChange={props.changeHandler} />
                    </Form.Group>
                    <Form.Group as={Col} controlId="filterNotes">
                        <Form.Label>{t('basic.notes')}</Form.Label>
                        <Form.Control name="notes" value={props.filterState.notes} onChange={props.changeHandler} />
                        <Form.Text className="text-muted">{t('sentence.searchEmptyString')}</Form.Text>
                    </Form.Group>
                    <Col md={2}>
                        <Form.Label>{t('basic.type')}</Form.Label>
                        <Form.Check
                            name="isSale"
                            label={t('basic.sale')}
                            defaultChecked={props.filterState.isSale}
                            onChange={props.changeHandler}
                        />
                        <Form.Check
                            name="isRestock"
                            label={t('basic.restock')}
                            defaultChecked={props.filterState.isRestock}
                            onChange={props.changeHandler}
                        />
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col} controlId="filterTotal">
                        <Form.Label>Total</Form.Label>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.from')}</InputGroup.Text>
                                <InputGroup.Text>Rp</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                name="totalFrom"
                                value={formatThousand(props.filterState.total.from)}
                                onChange={props.changeHandler}
                            />
                        </InputGroup>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.until')}</InputGroup.Text>
                                <InputGroup.Text>Rp</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                name="totalUntil"
                                value={formatThousand(props.filterState.total.until)}
                                onChange={props.changeHandler}
                            />
                        </InputGroup>
                    </Form.Group>
                    <Form.Group as={Col} controlId="filterDate">
                        <Form.Label>{t('basic.date')}</Form.Label>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.from')}</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                name="dateFrom"
                                type="date"
                                max={
                                    props.filterState.date.until
                                        ? moment(props.filterState.date.until).format('YYYY-MM-DD')
                                        : ''
                                }
                                value={
                                    props.filterState.date.from
                                        ? moment(props.filterState.date.from).format('YYYY-MM-DD')
                                        : ''
                                }
                                onChange={props.changeHandler}
                            />
                        </InputGroup>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.until')}</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                name="dateUntil"
                                type="date"
                                min={
                                    props.filterState.date.from
                                        ? moment(props.filterState.date.from).format('YYYY-MM-DD')
                                        : ''
                                }
                                value={
                                    props.filterState.date.until
                                        ? moment(props.filterState.date.until).format('YYYY-MM-DD')
                                        : ''
                                }
                                onChange={props.changeHandler}
                            />
                        </InputGroup>
                    </Form.Group>
                </Form.Row>
            </Col>
            <Form.Group as={Col} md={2}>
                <Form.Label>{t('basic.items')}</Form.Label>
                {items.map((item, index) => (
                    <Form.Check
                        key={index}
                        name="items"
                        type="checkbox"
                        value={item.id}
                        label={item.name}
                        defaultChecked={props.filterState.items.includes(item.id)}
                        onChange={props.changeHandler}
                    />
                ))}
            </Form.Group>
        </Form.Row>
    );
};

export default TransactionFilter;
