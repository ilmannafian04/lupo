import produce from 'immer';
import moment from 'moment';
import React, { ChangeEvent, FunctionComponent, useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { useTranslation } from 'react-i18next';

import LedgerHeader from './LedgerHeader';
import TransactionFilter, { TransactionFilterState } from './TransactionFilter';
import TransactionForm from './TransactionForm';
import TransactionTable from './TransactionTable';
import Transaction from '../../type/Transaction';

const Ledger: FunctionComponent = () => {
    const [formIsVisible, setFormIsVisible] = useState(false);
    const [filterIsVisible, setfilterIsVisible] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [transactions, setTransactions] = useState<Transaction[]>([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [pageCount, setPageCount] = useState(1);
    const [forceRefresh, setForceRefresh] = useState(false);
    const [filterState, setFilterState] = useState<TransactionFilterState>({
        id: '',
        notes: '',
        items: [],
        total: {
            from: '',
            until: '',
        },
        date: {
            from: '',
            until: '',
        },
        isSale: false,
        isRestock: false,
    });
    const { t } = useTranslation();
    useEffect(() => {
        let restock: boolean | '' = '';
        if (filterState.isRestock && !filterState.isSale) restock = true;
        else if (!filterState.isRestock && filterState.isSale) restock = false;
        window.ipcRenderer
            .invoke('transaction/get', {
                transactionPerPage: 10,
                page: currentPage,
                restock,
                count: true,
                ...filterState,
            })
            .then((result) => {
                setTransactions(result[0]);
                setPageCount(Math.ceil(result[1] / 10));
            })
            .catch((error) => console.error(error));
    }, [filterState, currentPage, forceRefresh]);
    useEffect(() => {
        if (currentPage > pageCount) setCurrentPage(pageCount);
    }, [pageCount, currentPage]);
    const filterChangeHandler = (event: ChangeEvent<HTMLInputElement>): void => {
        const target = event.target;
        setFilterState(
            produce(filterState, (draft) => {
                if (target.name === 'notes') {
                    draft.notes = target.value;
                } else if (target.name === 'items') {
                    const index = filterState.items.indexOf(Number(target.value));
                    if (index === -1) {
                        draft.items.push(Number(target.value));
                    } else {
                        draft.items.splice(index, 1);
                    }
                } else if (target.name === 'dateFrom' || target.name === 'dateUntil') {
                    const value: '' | Date = target.value ? moment(target.value, 'YYYY-MM-DD').toDate() : '';
                    if (target.name === 'dateFrom') {
                        draft.date.from = value;
                    } else {
                        draft.date.until = value;
                    }
                } else if (target.name === 'isSale') {
                    draft.isSale = !filterState.isSale;
                } else if (target.name === 'isRestock') {
                    draft.isRestock = !filterState.isRestock;
                } else {
                    let value = target.value.length === 0 ? '' : target.value;
                    if (target.name === 'id') {
                        draft.id = Number(value) || '';
                    } else {
                        if (value !== '') value = value.replace(/\./g, '');
                        if (target.name === 'totalFrom') {
                            draft.total.from = Number(value) || '';
                        } else {
                            draft.total.until = Number(value) || '';
                        }
                    }
                }
            }),
        );
    };
    return (
        <Container>
            <h1>{t('basic.ledger')}</h1>
            <div className="mb-3">
                <LedgerHeader />
            </div>
            <div className="mb-2">
                <Button
                    className="mr-1"
                    onClick={(): void => {
                        setFormIsVisible(!formIsVisible);
                    }}
                >
                    {formIsVisible ? t('sentence.hideForm') : t('sentence.showForm')}
                </Button>
                <Button
                    className="mr-1"
                    onClick={(): void => {
                        setfilterIsVisible(!filterIsVisible);
                    }}
                >
                    {filterIsVisible ? t('basic.hide') : t('basic.show')} filter
                </Button>
                <Button
                    className="mr-1"
                    onClick={(): void => {
                        setEditMode(!editMode);
                    }}
                >
                    {editMode ? t('basic.done') : t('basic.edit')}
                </Button>
            </div>
            {formIsVisible ? (
                <div className="mb-2">
                    <TransactionForm forceRefreshFn={(): void => setForceRefresh(!forceRefresh)} />
                </div>
            ) : null}
            {filterIsVisible ? (
                <div className="mb-2">
                    <TransactionFilter filterState={filterState} changeHandler={filterChangeHandler} />
                </div>
            ) : null}
            <TransactionTable
                transactions={transactions}
                editMode={editMode}
                currentPage={currentPage}
                pageCount={pageCount}
                changePageFn={(page: number): void => setCurrentPage(page)}
                forceRefreshFn={(): void => setForceRefresh(!forceRefresh)}
            />
        </Container>
    );
};

export default Ledger;
