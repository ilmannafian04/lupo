import React, { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import TransactionForm from './TransactionForm';
import StyledPrice from '../layout/StyledPrice';
import TableLayout from '../layout/TableLayout';
import UpdateFormModal from '../layout/UpdateFormModal';
import Transaction, { TransactionItem } from '../../type/Transaction';
import { formatDate } from '../../util/StringFormatting';
import notify from '../../util/Toast';

interface TransactionTableProps {
    editMode: boolean;
    transactions: Transaction[];
    currentPage: number;
    pageCount: number;
    changePageFn: (page: number) => void;
    forceRefreshFn: () => void;
}

const TransactionTable: FunctionComponent<TransactionTableProps> = (props: TransactionTableProps) => {
    const [editModalIsVisible, setEditModalIsVisible] = useState(false);
    const [editTransaction, setEditTransaction] = useState<Transaction>(undefined);
    const { t } = useTranslation();
    const deleteTransaction = (id: number): void => {
        window.ipcRenderer
            .invoke('delete-transaction', id)
            .then(() => {
                props.forceRefreshFn();
                notify(t('basic.success'));
            })
            .catch((error) => {
                notify(error, 'error');
                console.error(error);
            });
    };
    useEffect(() => {
        if (editTransaction) setEditModalIsVisible(true);
    }, [editTransaction]);
    useEffect(() => {
        if (!editModalIsVisible) setEditTransaction(undefined);
    }, [editModalIsVisible]);
    return (
        <>
            <TableLayout
                header={['ID', t('basic.notes'), t('basic.date'), t('basic.items'), 'Total']}
                data={props.transactions.map((transaction, index) => [
                    transaction.id.toString(),
                    transaction.notes,
                    formatDate(transaction.date),
                    transaction.items.reduce(
                        (itemString: string, transactionItem: TransactionItem) =>
                            transactionItem.item
                                ? itemString + `${transactionItem.count} x ${transactionItem.item.name}\n`
                                : itemString,
                        '',
                    ),
                    <StyledPrice key={index} isRestock={transaction.restock} price={transaction.total} />,
                ])}
                editMode={props.editMode}
                buttons={props.transactions.map((transaction) => [
                    {
                        name: t('basic.edit'),
                        variant: 'warning',
                        fn: (): void => setEditTransaction(transaction),
                    },
                    {
                        name: t('basic.delete'),
                        variant: 'danger',
                        fn: (): void => deleteTransaction(transaction.id),
                    },
                ])}
                currentPage={props.currentPage}
                pageCount={props.pageCount}
                changePageFn={props.changePageFn}
            />
            <UpdateFormModal
                hideHandler={(): void => setEditModalIsVisible(false)}
                isVisible={editModalIsVisible}
                form={<TransactionForm forceRefreshFn={props.forceRefreshFn} editTransaction={editTransaction} />}
                headerTitle={t('sentence.editTransaction')}
            />
        </>
    );
};

export default TransactionTable;
