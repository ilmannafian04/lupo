import React, { FunctionComponent, useEffect, useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useTranslation } from 'react-i18next';

import { formatCurrency, formatDate } from '../../util/StringFormatting';

const LedgerHeader: FunctionComponent = () => {
    const [todaysSale, setTodaysSale] = useState(0);
    const [todaysRestock, setTodaysRestock] = useState(0);
    const [balance, setBalance] = useState(0);
    const { t } = useTranslation();
    useEffect(() => {
        window.ipcRenderer
            .invoke('ledger/todaysSummary')
            .then((result) => {
                setTodaysSale(result[0].todaysSale ? result[0].todaysSale : 0);
                setTodaysRestock(result[1].todaysRestock ? result[1].todaysRestock : 0);
                const sale = result[2].sale ? result[2].sale : 0;
                const restock = result[3].restock ? result[3].restock : 0;
                const balance = result[4].balance ? result[4].balance : 0;
                setBalance(sale - restock + balance);
            })
            .catch((error) => console.error(error));
    }, []);
    return (
        <>
            <h4>{t('sentence.todaysSummary')}</h4>
            <Row className="mb-2">
                <Col>
                    <span>
                        <strong>{t('basic.date')}:</strong> {formatDate(new Date())}
                    </span>
                </Col>
                <Col>
                    <span>
                        <strong>{t('basic.sale')}:</strong> {formatCurrency(todaysSale)}
                    </span>
                </Col>
                <Col>
                    <span>
                        <strong>{t('basic.restock')}:</strong> {formatCurrency(todaysRestock)}
                    </span>
                </Col>
                <Col>
                    <span>
                        <strong>{t('sentence.todaysBalance')}:</strong> {formatCurrency(balance)}
                    </span>
                </Col>
            </Row>
        </>
    );
};

export default LedgerHeader;
