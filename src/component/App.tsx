import React, { FunctionComponent } from 'react';
import { Route, Switch } from 'react-router-dom';

import ItemStock from './itemStock/ItemStock';
import NavBar from './layout/NavBar';
import Ledger from './ledger/Ledger';
import Manage from './manage/Manage';
import YearBook from './yearBook/YearBook';

const App: FunctionComponent = () => {
    return (
        <div>
            <NavBar />
            <Switch>
                <Route path="/manage">
                    <Manage />
                </Route>
                <Route path="/items">
                    <ItemStock />
                </Route>
                <Route path="/yearbook">
                    <YearBook />
                </Route>
                <Route path="/">
                    <Ledger />
                </Route>
            </Switch>
        </div>
    );
};

export default App;
