import React, { ChangeEvent, FormEvent, FunctionComponent, useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { useTranslation } from 'react-i18next';

import { formatThousand } from '../../util/StringFormatting';

const BalanceForm: FunctionComponent = () => {
    const [formState, setFormState] = useState<number | ''>('');
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [currentBalance, setCurrentBalance] = useState(0);
    const { t } = useTranslation();
    useEffect(() => {
        window.ipcRenderer
            .invoke('balance/get')
            .then((balance) => setCurrentBalance(balance[0].balance))
            .catch((error) => console.error(error));
    }, []);
    const formChangeHandler = (event: ChangeEvent<HTMLInputElement>): void => {
        const target = event.target;
        if (target.value.length === 0) {
            setFormState('');
        } else if (/^[\d.]+$/.test(target.value)) {
            setFormState(Number(target.value.replace(/\./g, '')));
        }
    };
    const submitHandler = (event: FormEvent<HTMLFormElement>): void => {
        setIsSubmitting(true);
        event.preventDefault();
        window.ipcRenderer
            .invoke('balance/set', formState)
            .then(() => {
                setCurrentBalance(formState || 0);
                setFormState('');
            })
            .catch((error) => console.error(error))
            .then(() => setIsSubmitting(false));
    };
    return (
        <Form onSubmit={submitHandler}>
            <Form.Group controlId="balanceAmount">
                <Form.Label>{t('basic.amount')}</Form.Label>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Rp</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control
                        name="amount"
                        placeholder={formatThousand(currentBalance)}
                        value={formatThousand(formState)}
                        onChange={formChangeHandler}
                    />
                    <InputGroup.Append>
                        <Button type="submit" disabled={isSubmitting}>
                            {t('basic.submit')}
                        </Button>
                    </InputGroup.Append>
                </InputGroup>
            </Form.Group>
        </Form>
    );
};

export default BalanceForm;
