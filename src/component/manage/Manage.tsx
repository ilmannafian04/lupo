import React, { FunctionComponent } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { useTranslation } from 'react-i18next';

import ManageCategory from './category/ManageCategory';
import InitialBalanceForm from './InitialBalanceForm';
import notify from '../../util/Toast';

const exportTransactionHandler = (): void => {
    window.ipcRenderer
        .invoke('export-database')
        .then(() => notify('Success'))
        .catch((error) => console.error(error));
};

const Manage: FunctionComponent = () => {
    const { t } = useTranslation();
    return (
        <Container>
            <div className="mt-3">
                <ManageCategory />
            </div>
            <div className="mt-3">
                <h4>{t('sentence.initialBalance')}</h4>
                <InitialBalanceForm />
            </div>
            <div className="mt-3">
                <h4>{t('sentence.exportTransactions')}</h4>
                <Button onClick={exportTransactionHandler}>{t('basic.export')}</Button>
            </div>
        </Container>
    );
};

export default Manage;
