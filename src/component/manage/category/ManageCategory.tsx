import React, { FunctionComponent, useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import { useTranslation } from 'react-i18next';

import CategoryForm from './CategoryForm';
import CategoryTable from './CategoryTable';
import Category from '../../../type/Category';

const ManageCategory: FunctionComponent = () => {
    const [categoryFormIsVisible, setCategoryFormIsVisible] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [categories, setCategories] = useState<Category[]>([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [pageCount, setPageCount] = useState(1);
    const [forceRefresh, setForceRefresh] = useState(false);
    const { t } = useTranslation();
    useEffect(() => {
        window.ipcRenderer
            .invoke('category/get', { categoryPerPage: 10, page: currentPage, count: true })
            .then((result) => {
                setCategories(result[0]);
                setPageCount(Math.ceil(result[1] / 10));
            })
            .catch((error) => console.error(error));
    }, [currentPage, forceRefresh]);
    useEffect(() => {
        if (currentPage > pageCount) setCurrentPage(pageCount);
    }, [pageCount, currentPage]);
    return (
        <>
            <h4>{t('sentence.manageCategory')}</h4>
            <div className="mb-2">
                <Button
                    className="mr-1"
                    onClick={(): void => {
                        setCategoryFormIsVisible(!categoryFormIsVisible);
                    }}
                >
                    {categoryFormIsVisible ? t('sentence.hideForm') : t('sentence.showForm')}
                </Button>
                <Button
                    className="mr-1"
                    onClick={(): void => {
                        setEditMode(!editMode);
                    }}
                >
                    {editMode ? t('basic.done') : t('basic.edit')}
                </Button>
            </div>
            {categoryFormIsVisible ? (
                <div className="mb-2">
                    <CategoryForm forceRefreshFn={(): void => setForceRefresh(!forceRefresh)} />
                </div>
            ) : null}
            <CategoryTable
                categories={categories}
                editMode={editMode}
                currentPage={currentPage}
                pageCount={pageCount}
                changePageFn={(page: number): void => setCurrentPage(page)}
                forceRefreshFn={(): void => setForceRefresh(!forceRefresh)}
            />
        </>
    );
};

export default ManageCategory;
