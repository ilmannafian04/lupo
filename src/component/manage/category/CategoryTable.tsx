import React, { FunctionComponent, useState } from 'react';
import { useTranslation } from 'react-i18next';

import CategoryForm from './CategoryForm';
import TableLayout from '../../layout/TableLayout';
import UpdateFormModal from '../../layout/UpdateFormModal';
import Category from '../../../type/Category';
import notify from '../../../util/Toast';

interface CategoryTableProps {
    categories: Category[];
    editMode: boolean;
    currentPage: number;
    pageCount: number;
    changePageFn: (page: number) => void;
    forceRefreshFn: () => void;
}

const CategoryTable: FunctionComponent<CategoryTableProps> = (props: CategoryTableProps) => {
    const [editCategory, setEditCategory] = useState<Category>(undefined);
    const [editModalIsVisible, setEditModalIsVisible] = useState(false);
    const { t } = useTranslation();
    const deleteItem = (id: number): void => {
        window.ipcRenderer
            .invoke('delete-category', id)
            .then(() => {
                props.forceRefreshFn();
                notify(t('basic.success'));
            })
            .catch((error) => {
                console.error(error);
                notify(error, 'error');
            });
    };
    return (
        <>
            <TableLayout
                header={['ID', t('basic.name')]}
                data={props.categories.map((category) => [category.id.toString(), category.name])}
                editMode={props.editMode}
                buttons={props.categories.map((category) => {
                    return [
                        {
                            name: t('basic.edit'),
                            variant: 'warning',
                            fn: (): void => {
                                setEditCategory(category);
                                setEditModalIsVisible(true);
                            },
                        },
                        {
                            name: t('basic.delete'),
                            variant: 'danger',
                            fn: (): void => {
                                deleteItem(category.id);
                            },
                        },
                    ];
                })}
                currentPage={props.currentPage}
                pageCount={props.pageCount}
                changePageFn={props.changePageFn}
            />
            <UpdateFormModal
                hideHandler={(): void => {
                    setEditModalIsVisible(false);
                    setEditCategory(undefined);
                }}
                isVisible={editModalIsVisible}
                form={<CategoryForm forceRefreshFn={props.forceRefreshFn} editCategory={editCategory} />}
                headerTitle={t('sentence.editCategory')}
            />
        </>
    );
};

export default CategoryTable;
