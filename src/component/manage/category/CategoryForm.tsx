import React, { ChangeEvent, FormEvent, FunctionComponent, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';

import Category from '../../../type/Category';
import notify from '../../../util/Toast';

interface CategoryFormProps {
    forceRefreshFn: () => void;
    editCategory?: Category;
}

const formValidator = (name: string): boolean => {
    return name.length > 0;
};

const CategoryForm: FunctionComponent<CategoryFormProps> = (props: CategoryFormProps) => {
    const [categoryName, setCategoryName] = useState<string>(props.editCategory ? props.editCategory.name : '');
    const [submitting, setSubmitting] = useState(false);
    const { t } = useTranslation();
    const inputChangeHandler = (event: ChangeEvent<HTMLInputElement>): void => {
        setCategoryName(event.target.value);
    };
    const formSubmitHandler = (event: FormEvent<HTMLFormElement>): void => {
        event.preventDefault();
        setSubmitting(true);
        window.ipcRenderer
            .invoke(`${props.editCategory ? 'update' : 'submit'}-category`, {
                id: props.editCategory ? props.editCategory.id : undefined,
                name: categoryName,
            })
            .then(() => {
                if (!props.editCategory) setCategoryName('');
                props.forceRefreshFn();
                notify(t('basic.success'));
            })
            .catch((error) => {
                console.error(error);
                notify(error, 'error');
            })
            .then(() => setSubmitting(false));
    };
    return (
        <>
            <Form onSubmit={formSubmitHandler}>
                <Form.Group controlId="categoryName">
                    <Form.Label>{t('basic.name')}</Form.Label>
                    <Form.Control name="name" value={categoryName} onChange={inputChangeHandler} />
                </Form.Group>
                <Button disabled={!formValidator(categoryName) && !submitting} type="submit">
                    {t('basic.submit')}
                </Button>
            </Form>
        </>
    );
};

export default CategoryForm;
