import React, { ChangeEvent, useEffect, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';

import YearBookTable from './YearBookTable';

const YearBook: React.FunctionComponent = () => {
    const [selectedYear, setSelectedYear] = useState('0000');
    const [years, setYears] = useState<string[]>([]);
    const { t } = useTranslation();
    useEffect(() => {
        window.ipcRenderer
            .invoke('yearBook/yearList')
            .then((result) => setYears(result.map((year: { year: string }) => year.year)))
            .catch((error) => console.error(error));
    }, []);
    return (
        <Container>
            <h1>{t('basic.yearBook')}</h1>
            <Form.Group style={{ maxWidth: '8rem' }}>
                <Form.Control
                    as="select"
                    value={selectedYear}
                    onChange={(event: ChangeEvent<HTMLSelectElement>): void => setSelectedYear(event.target.value)}
                >
                    <option value="0000">{t('basic.pick')}</option>
                    {years.map((year, index) => (
                        <option key={index} value={year}>
                            {year}
                        </option>
                    ))}
                </Form.Control>
            </Form.Group>
            <YearBookTable year={selectedYear} />
        </Container>
    );
};

export default YearBook;
