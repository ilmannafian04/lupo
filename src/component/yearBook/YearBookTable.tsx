import moment from 'moment';
import 'moment/locale/id';
import React, { useEffect, useState } from 'react';
import Table from 'react-bootstrap/Table';
import { useTranslation } from 'react-i18next';

import Item from '../../type/Item';
import { formatCurrency } from '../../util/StringFormatting';

interface YearBookTableProps {
    year: string;
}

interface YearBookData {
    total: number;
    restock: number;
    month: string;
    name: string;
}

const MONTHS_IN_NUM = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

const YearBookTable: React.FunctionComponent<YearBookTableProps> = (props: YearBookTableProps) => {
    const [datas, setData] = useState<YearBookData[]>([]);
    const [items, setItems] = useState<Item[]>([]);
    const { t, i18n } = useTranslation();
    useEffect(() => {
        const promises = [
            window.ipcRenderer.invoke('yearBook/getData', props.year),
            window.ipcRenderer.invoke('item/get'),
        ];
        Promise.all(promises)
            .then((results) => {
                setData(results[0]);
                setItems(results[1]);
            })
            .catch((error) => console.error(error));
    }, [props.year]);
    return (
        <Table bordered={true} striped={true} responsive>
            <thead>
                <tr>
                    <th rowSpan={2} style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                        {t('basic.month')}
                    </th>
                    {items.map((item, index) => (
                        <th colSpan={2} key={index} style={{ textAlign: 'center' }}>
                            {item.name}
                        </th>
                    ))}
                </tr>
                <tr>
                    {items.map((item, index) => (
                        <React.Fragment key={index}>
                            <th style={{ textAlign: 'center' }}>{t('basic.sale')}</th>
                            <th style={{ textAlign: 'center' }}>{t('basic.restock')}</th>
                        </React.Fragment>
                    ))}
                </tr>
            </thead>
            <tbody>
                {MONTHS_IN_NUM.map((month, monthIndex) => {
                    const localMoment = moment(month, 'MM');
                    localMoment.locale(i18n.languages[0]);
                    return (
                        <tr key={monthIndex}>
                            <td>{localMoment.format('MMMM')}</td>
                            {items.map((item, itemIndex) =>
                                [0, 1].map((restock, restockIndex) => {
                                    const data = datas.find(
                                        (data) =>
                                            data.month === month && data.name === item.name && data.restock === restock,
                                    );
                                    return (
                                        <td key={`${itemIndex}-${restockIndex}`} style={{ textAlign: 'right' }}>
                                            {data ? formatCurrency(data.total) : null}
                                        </td>
                                    );
                                }),
                            )}
                        </tr>
                    );
                })}
            </tbody>
        </Table>
    );
};

export default YearBookTable;
