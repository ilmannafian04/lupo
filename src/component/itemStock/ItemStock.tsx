import produce from 'immer';
import React, { ChangeEvent, FunctionComponent, useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { useTranslation } from 'react-i18next';

import ItemFilter, { ItemFilterState } from './ItemFilter';
import ItemForm from './ItemForm';
import ItemTable from './ItemTable';
import Item from '../../type/Item';

const ItemStock: FunctionComponent = () => {
    const [itemFormIsVisible, setItemFormIsVisible] = useState<boolean>(false);
    const [filterIsVisible, setFilterIsVisible] = useState<boolean>(false);
    const [editMode, setEditMode] = useState<boolean>(false);
    const [items, setItems] = useState<Item[]>([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [pageCount, setPageCount] = useState(1);
    const [forceRefresh, setForceRefresh] = useState(false);
    const [filterState, setFilterState] = useState<ItemFilterState>({
        id: '',
        name: '',
        stock: {
            from: '',
            until: '',
        },
        price: {
            from: '',
            until: '',
        },
        restockPrice: {
            from: '',
            until: '',
        },
        categories: [],
    });
    const { t } = useTranslation();
    useEffect(() => {
        window.ipcRenderer
            .invoke('item/get', { itemPerPage: 10, page: currentPage, count: true, ...filterState })
            .then((result) => {
                setItems(result[0]);
                setPageCount(Math.ceil(result[1] / 10));
            })
            .catch((error) => console.error(error));
    }, [filterState, currentPage, forceRefresh]);
    useEffect(() => {
        if (currentPage > pageCount) setCurrentPage(pageCount);
    }, [pageCount, currentPage]);
    const updateFilter = (event: ChangeEvent<HTMLInputElement>): void => {
        const target = event.target;
        setFilterState(
            produce(filterState, (draft) => {
                if (target.name === 'name') {
                    draft.name = target.value;
                } else if (target.name === 'category') {
                    const index = filterState.categories.indexOf(Number(target.value));
                    if (index === -1) {
                        draft.categories.push(Number(target.value));
                    } else {
                        draft.categories.splice(index, 1);
                    }
                } else {
                    if (target.value.match(/\d+/) || target.value === '') {
                        if (target.value.includes('.')) target.value = target.value.replace(/\./g, '');
                        const value = target.value ? Number(target.value) : '';
                        switch (target.name) {
                            case 'id': {
                                draft.id = value;
                                break;
                            }
                            case 'stockFrom': {
                                draft.stock.from = value;
                                break;
                            }
                            case 'stockUntil': {
                                draft.stock.until = value;
                                break;
                            }
                            case 'salePriceFrom': {
                                draft.price.from = value;
                                break;
                            }
                            case 'salePriceUntil': {
                                draft.price.until = value;
                                break;
                            }
                            case 'restockPriceFrom': {
                                draft.restockPrice.from = value;
                                break;
                            }
                            case 'restockPriceUntil': {
                                draft.restockPrice.until = value;
                                break;
                            }
                        }
                    }
                }
            }),
        );
    };
    return (
        <Container>
            <h1>{t('sentence.itemStock')}</h1>
            <div className="mb-2">
                <Button
                    className="mr-1"
                    onClick={(): void => {
                        setItemFormIsVisible(!itemFormIsVisible);
                    }}
                >
                    {itemFormIsVisible ? t('sentence.hideForm') : t('sentence.showForm')}
                </Button>
                <Button
                    className="mr-1"
                    onClick={(): void => {
                        setFilterIsVisible(!filterIsVisible);
                    }}
                >
                    {filterIsVisible ? t('basic.hide') : t('basic.show')} filter
                </Button>
                <Button
                    className="mr-1"
                    onClick={(): void => {
                        setEditMode(!editMode);
                    }}
                >
                    {editMode ? t('basic.done') : t('basic.edit')}
                </Button>
            </div>
            {itemFormIsVisible ? (
                <div className="mb-2">
                    <ItemForm forceRefreshFn={(): void => setForceRefresh(!forceRefresh)} />
                </div>
            ) : null}
            {filterIsVisible ? (
                <div className="mb-2">
                    {' '}
                    <ItemFilter updateFunction={updateFilter} filterState={filterState} />
                </div>
            ) : null}
            <ItemTable
                items={items}
                editMode={editMode}
                currentPage={currentPage}
                pageCount={pageCount}
                changePageFn={(page: number): void => setCurrentPage(page)}
                forceRefreshFn={(): void => setForceRefresh(!forceRefresh)}
            />
        </Container>
    );
};

export default ItemStock;
