import React, { ChangeEvent, FunctionComponent, useEffect, useState } from 'react';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { useTranslation } from 'react-i18next';

import Category from '../../type/Category';
import FilterRange from '../../type/FilterRange';
import { formatThousand } from '../../util/StringFormatting';

export interface ItemFilterState {
    id: number | '';
    name: string;
    stock: FilterRange<number | ''>;
    price: FilterRange<number | ''>;
    restockPrice: FilterRange<number | ''>;
    categories: number[];
}

interface ItemFilterProps {
    updateFunction: (event: ChangeEvent<HTMLInputElement>) => void;
    filterState: ItemFilterState;
}

const ItemFilter: FunctionComponent<ItemFilterProps> = (props: ItemFilterProps) => {
    const [categories, setCategories] = useState<Category[]>([]);
    const { t } = useTranslation();
    useEffect(() => {
        window.ipcRenderer
            .invoke('category/get')
            .then((result) => setCategories(result))
            .catch((error) => console.error(error));
    }, []);
    return (
        <Form.Row>
            <Col>
                <Form.Row>
                    <Form.Group as={Col} controlId="filterId" md={2}>
                        <Form.Label>ID</Form.Label>
                        <Form.Control name="id" onChange={props.updateFunction} value={props.filterState.id} />
                    </Form.Group>
                    <Form.Group as={Col} controlId="filterName">
                        <Form.Label>{t('basic.name')}</Form.Label>
                        <Form.Control name="name" onChange={props.updateFunction} value={props.filterState.name} />
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col} controlId="filterSalePrice">
                        <Form.Label>{t('sentence.salePrice')}</Form.Label>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.from')}</InputGroup.Text>
                                <InputGroup.Text>Rp</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                name="salePriceFrom"
                                onChange={props.updateFunction}
                                value={formatThousand(props.filterState.price.from)}
                            />
                        </InputGroup>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.until')}</InputGroup.Text>
                                <InputGroup.Text>Rp</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                name="salePriceUntil"
                                onChange={props.updateFunction}
                                value={formatThousand(props.filterState.price.until)}
                            />
                        </InputGroup>
                    </Form.Group>
                    <Form.Group as={Col} controlId="filterRestockPrice">
                        <Form.Label>{t('sentence.restockPrice')}</Form.Label>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.from')}</InputGroup.Text>
                                <InputGroup.Text>Rp</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                name="restockPriceFrom"
                                onChange={props.updateFunction}
                                value={formatThousand(props.filterState.restockPrice.from)}
                            />
                        </InputGroup>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.until')}</InputGroup.Text>
                                <InputGroup.Text>Rp</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                name="restockPriceUntil"
                                onChange={props.updateFunction}
                                value={formatThousand(props.filterState.restockPrice.until)}
                            />
                        </InputGroup>
                    </Form.Group>
                    <Form.Group as={Col} controlId="filterStock">
                        <Form.Label>{t('basic.stock')}</Form.Label>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.from')}</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                type="number"
                                name="stockFrom"
                                onChange={props.updateFunction}
                                value={props.filterState.stock.from}
                            />
                        </InputGroup>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>{t('basic.until')}</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                type="number"
                                name="stockUntil"
                                onChange={props.updateFunction}
                                value={props.filterState.stock.until}
                            />
                        </InputGroup>
                    </Form.Group>
                </Form.Row>
            </Col>
            <Form.Group as={Col} md={2}>
                <Form.Label>{t('basic.category')}</Form.Label>
                {categories.map((category, index) => (
                    <Form.Check
                        name="category"
                        key={index}
                        type="checkbox"
                        label={category.name}
                        value={category.id}
                        onChange={props.updateFunction}
                        defaultChecked={props.filterState.categories.includes(category.id)}
                    />
                ))}
            </Form.Group>
        </Form.Row>
    );
};

export default ItemFilter;
