import produce from 'immer';
import React, { ChangeEvent, FormEvent, FunctionComponent, useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { useTranslation } from 'react-i18next';

import Category from '../../type/Category';
import Item from '../../type/Item';
import notify from '../../util/Toast';

interface ItemFormProps {
    forceRefreshFn: () => void;
    editItem?: Item;
}

interface ItemFormState {
    name: string;
    price: number;
    restockPrice: number;
    category: number;
}

const initialFormState: ItemFormState = { name: '', price: 0, restockPrice: 0, category: -1 };
const formValidator = (data: ItemFormState): boolean => {
    if (data.name.length === 0) return false;
    if (data.price < 0) return false;
    if (data.restockPrice < 0) return false;
    return data.category !== -1;
};

const ItemForm: FunctionComponent<ItemFormProps> = (props: ItemFormProps) => {
    const [isSubbmitting, setIsSubmitting] = useState(false);
    const [formState, setFormState] = useState<ItemFormState>(
        props.editItem === undefined
            ? initialFormState
            : {
                  name: props.editItem.name,
                  price: props.editItem.price,
                  restockPrice: props.editItem.restockPrice,
                  category: props.editItem.category.id,
              },
    );
    const { t } = useTranslation();
    const [categories, setCategories] = useState<Category[]>([]);
    useEffect(() => {
        window.ipcRenderer
            .invoke('category/get')
            .then((result) => setCategories(result))
            .catch((error) => console.error(error));
    }, []);
    useEffect(() => {
        if (formState.category === -1 && categories.length > 0)
            setFormState(
                produce(formState, (draft) => {
                    draft.category = categories[0].id;
                }),
            );
    }, [formState, categories]);
    const formChangeHandler = (event: ChangeEvent<HTMLSelectElement | HTMLInputElement>): void => {
        const target = event.target;
        if (target.name === 'price' || target.name === 'restockPrice') {
            target.value = target.value.replace(/\./g, '');
            if (target.value.length === 0) target.value = '0';
            if (!/^\d*$/.test(target.value)) return;
        }
        setFormState(
            produce(formState, (draft) => {
                switch (target.name) {
                    case 'name': {
                        draft.name = target.value;
                        break;
                    }
                    case 'price': {
                        draft.price = Number(target.value.replace(/\./g, ''));
                        break;
                    }
                    case 'restockPrice': {
                        draft.restockPrice = Number(target.value.replace(/\./g, ''));
                        break;
                    }
                    case 'category': {
                        draft.category = Number(target.value);
                        break;
                    }
                }
            }),
        );
    };
    const formSubmitHandler = (event: FormEvent<HTMLFormElement>): void => {
        event.preventDefault();
        setIsSubmitting(true);
        const dto = {
            id: props.editItem ? props.editItem.id : undefined,
            ...formState,
        };
        window.ipcRenderer
            .invoke(props.editItem ? 'update-item' : 'submit-item', dto)
            .then(() => {
                if (!props.editItem) setFormState(initialFormState);
                props.forceRefreshFn();
                notify(t('basic.success'));
            })
            .catch((error) => {
                notify(error, 'error');
                console.error(error);
            })
            .then(() => setIsSubmitting(false));
    };
    return (
        <Form onSubmit={formSubmitHandler}>
            <Form.Row>
                <Form.Group as={Col} controlId="itemName">
                    <Form.Label>{t('basic.name')}</Form.Label>
                    <Form.Control name="name" value={formState.name} onChange={formChangeHandler} />
                </Form.Group>
                <Form.Group as={Col} controlId="itemPrice">
                    <Form.Label>{t('sentence.salePrice')}</Form.Label>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text>Rp</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control
                            name="price"
                            placeholder="Item price"
                            value={formState.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                            onChange={formChangeHandler}
                        />
                    </InputGroup>
                </Form.Group>
                <Form.Group as={Col} controlId="itemPrice">
                    <Form.Label>{t('sentence.restockPrice')}</Form.Label>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text>Rp</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control
                            name="restockPrice"
                            placeholder="Restock price"
                            value={formState.restockPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                            onChange={formChangeHandler}
                        />
                    </InputGroup>
                </Form.Group>
                <Form.Group as={Col} controlId="itemCategory">
                    <Form.Label> {t('basic.category')}</Form.Label>
                    <Form.Control value={formState.category} as="select" name="category" onChange={formChangeHandler}>
                        {categories.map((category, index) => (
                            <option key={index} value={category.id}>
                                {category.name}
                            </option>
                        ))}
                    </Form.Control>
                </Form.Group>
            </Form.Row>
            <Button disabled={!formValidator(formState) && !isSubbmitting} type="submit">
                {t('basic.submit')}
            </Button>
        </Form>
    );
};

export default ItemForm;
