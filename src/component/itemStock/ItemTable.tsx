import React, { FunctionComponent, useState } from 'react';
import { useTranslation } from 'react-i18next';

import ItemForm from './ItemForm';
import TableLayout from '../layout/TableLayout';
import UpdateFormModal from '../layout/UpdateFormModal';
import Item from '../../type/Item';
import { formatCurrency } from '../../util/StringFormatting';
import notify from '../../util/Toast';

interface ItemTableProps {
    items: Item[];
    editMode: boolean;
    currentPage: number;
    pageCount: number;
    changePageFn: (page: number) => void;
    forceRefreshFn: () => void;
}

export const ItemTable: FunctionComponent<ItemTableProps> = (props: ItemTableProps) => {
    const [editItem, setEditItem] = useState<Item>(undefined);
    const [editModalIsVisible, setEditModalIsVisible] = useState(false);
    const { t } = useTranslation();
    const deleteItem = (id: number): void => {
        window.ipcRenderer
            .invoke('delete-item', id)
            .then(() => {
                props.forceRefreshFn();
                notify(t('basic.success'));
            })
            .catch((error) => {
                console.error(error);
                notify(error, 'error');
            });
    };
    return (
        <>
            <TableLayout
                header={[
                    'ID',
                    t('basic.name'),
                    t('sentence.salePrice'),
                    t('sentence.restockPrice'),
                    t('basic.stock'),
                    t('basic.category'),
                ]}
                data={props.items.map((item) => [
                    item.id.toString(),
                    item.name,
                    formatCurrency(item.price),
                    formatCurrency(item.restockPrice),
                    item.stock.toString(),
                    item.category.name,
                ])}
                editMode={props.editMode}
                buttons={props.items.map((item) => {
                    return [
                        {
                            name: t('basic.edit'),
                            variant: 'warning',
                            fn: (): void => {
                                setEditItem(item);
                                setEditModalIsVisible(true);
                            },
                        },
                        {
                            name: t('basic.delete'),
                            variant: 'danger',
                            fn: (): void => {
                                deleteItem(item.id);
                            },
                        },
                    ];
                })}
                currentPage={props.currentPage}
                pageCount={props.pageCount}
                changePageFn={props.changePageFn}
            />
            <UpdateFormModal
                hideHandler={(): void => {
                    setEditModalIsVisible(false);
                    setEditItem(undefined);
                }}
                isVisible={editModalIsVisible}
                form={<ItemForm forceRefreshFn={props.forceRefreshFn} editItem={editItem} />}
                headerTitle={t('sentence.editItem')}
            />
        </>
    );
};

export default ItemTable;
