import { toast } from 'react-toastify';

const notify = (message: string, type = 'success'): void => {
    switch (type) {
        case 'success': {
            toast.info(message);
            break;
        }
        case 'error': {
            toast.error(message);
        }
    }
};

export default notify;
