import i18next from 'i18next';
import moment from 'moment';

export const formatThousand = (num: number | string): string => {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
};

export function formatCurrency(amount: number): string {
    return 'Rp' + formatThousand(amount);
}

export function formatDate(date: Date | string): string {
    return `${i18next.t('day.name', { returnObjects: true })[moment(date).toDate().getDay()]}, ${moment(date).format(
        'DD/MM/YY',
    )}`;
}

export function fCap(string: string): string {
    return string[0].toUpperCase() + string.slice(1);
}
