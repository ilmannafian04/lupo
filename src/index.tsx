import 'bootstrap/dist/css/bootstrap.min.css';
import { IpcRenderer } from 'electron';
import { ElectronLog } from 'electron-log';
import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
// import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import App from './component/App';
import './i18n';
import './index.css';
// import store from './store/RootStore';

declare global {
    interface Window {
        ipcRenderer: IpcRenderer;
        elog: ElectronLog;
    }
}

Object.assign(console, window.elog.functions);
window.ipcRenderer.invoke('log/getPath').then((logPath) => {
    window.elog.transports.file.resolvePath = (): string => {
        return logPath;
    };
    window.elog.transports.file.archiveLog = async (file): Promise<void> => {
        await window.ipcRenderer.invoke('log/rotate', file);
    };
    console.log('React starting');
    ReactDOM.render(
        // <Provider store={store}>
        <BrowserRouter>
            <Suspense fallback={<h1>Loading</h1>}>
                <App />
                <ToastContainer position="bottom-right" newestOnTop closeOnClick />
            </Suspense>
        </BrowserRouter>,
        // </Provider>,
        document.getElementById('root'),
    );
});

export const { ipcRenderer } = window;
